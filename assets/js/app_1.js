// JavaScript Document

/* ************************************************************************************************************************

Jardín Dream Home Kindergarten

File:			app_1.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {});

// Form

jQuery( '#submit' ).click(function ( event ) {
	event.preventDefault();
});

function myFunction() {
	var nombre = document.getElementById( 'nombre' ).value;
	var email = document.getElementById( 'email' ).value;
	var celular = document.getElementById( 'celular' ).value;
	var dataString =
		'nombre1=' + nombre +
		'&email1=' + email +
		'&celular1=' + celular
	;
	console.log( dataString );
	if ( nombre === '' || email === '' || celular === '' ) {
		alert( 'Por favor ingrese todos los campos obligatorios.' );
	} else {
		jQuery.ajax({
			type: 'POST',
			url: 'build_1/app.php',
			data: dataString,
			cache: false,
			success: function ( html ) {
				alert( html );
				//jQuery( '#myModal' ).modal( 'show' );
			}
		});
		jQuery( '#form' ).each(function () {
			this.reset();
		});
	}
	return false;
}