// JavaScript Document

/* ************************************************************************************************************************

Jardín Dream Home Kindergarten

File:			Gulpfile.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyHTML = require('gulp-htmlmin');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');

// HTML

gulp.task('html', function () {
	return gulp
		.src('assets/html/app.html')
		.pipe(minifyHTML({
			collapseWhitespace: true,
			removeComments: true
		}))
		.pipe(rename('index.html'))
		.pipe(gulp.dest(''));
});

gulp.task('html_no', function () {
	return gulp
		.src('assets/html/app_no.html')
		.pipe(minifyHTML({
			collapseWhitespace: true,
			removeComments: true
		}))
		.pipe(rename('index_no.html'))
		.pipe(gulp.dest(''));
});

gulp.task('html_1', function () {
	return gulp
		.src('assets/html/app_1.html')
		.pipe(minifyHTML({
			collapseWhitespace: true,
			removeComments: true
		}))
		.pipe(rename('index_1.html'))
		.pipe(gulp.dest(''));
});

// CSS

gulp.task('css', function () {
	return gulp
		.src('assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('build'));
});

gulp.task('css_1', function () {
	return gulp
		.src('assets/css/app_1.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('build_1'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('build'));
});

gulp.task('js_1', function () {
	return gulp
		.src('assets/js/app_1.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('build_1'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('build'));
});

gulp.task('image_1', function () {
	return gulp
		.src('assets/images_1/*')
		.pipe(image())
		.pipe(gulp.dest('build_1'));
});

// Watch

gulp.task('watch', function () {
	gulp.watch(['assets/html/**/*.html'], ['html']);
	gulp.watch(['assets/css/**/*.scss'], ['css']);
	gulp.watch(['assets/js/**/*.js'], ['js']);
});

// Default

gulp.task('default', ['html', 'html_no', 'html_1', 'css', 'css_1', 'js', 'js_1', 'image', 'image_1', 'watch']);